import java.util.Random;

public class RouletteWheel {

    private Random ran;
    private int number = 0;

    public RouletteWheel() {
        this.ran = new Random();
        spin();
    }

    public void spin() {
        this.number = ran.nextInt(37);
    }

    public int getValue() {
        return this.number;
    }
}