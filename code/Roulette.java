import java.util.Scanner;

public class Roulette {
    
    public static void main (String[] args) {

        RouletteWheel rw = new RouletteWheel();
        int money = 1000;
        Scanner input = new Scanner(System.in);
        String userInput = "";
        int betAmount = 0;
        int moneyModified = 0;
        do {
            //prompt if participate 
            System.out.println("Would you like to make a bet? If yes, enter a number. Press enter to quit");
            userInput = input.nextLine();
            if(userInput.isBlank()) {
                System.out.println("You chosed to quit");
                break;
            }
            
            //prompt for bet value 
            System.out.println("Enter amount of bet");
            betAmount = input.nextInt();

            //check for entered value
            if(betAmount > money) {
                System.out.println("You don't have enough money");
                break; //could also do continue here
            }

            //generate number
            rw.spin();

            //modify money
            int modifier = 0;
            if(determineWon(Integer.parseInt(userInput), rw.getValue())) {
                modifier += betAmount * rw.getValue() - 1;
            }else {
                modifier -= betAmount;
            }
            money += modifier;
            moneyModified += modifier;
            //indicate results
            System.out.println("Your balance " + modifier + " for this round");
            System.out.println("Your current balance is " + money + "$");

            input.nextLine();
        }while(money > 0);
        System.out.println("Your original balance " + moneyModified + " after all rounds");
        System.out.println("Your exit balance is " + money + "$");
    }

    public static boolean determineWon(int userNum, int rouletteWheel) {
        return userNum == rouletteWheel ? true : false;
    }
}
